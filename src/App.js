import { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { addUser, fetchUsers } from './reducers/userSlice';

function App() {
  const { users, loadingUsers, error } = useSelector(state => state.user)
  const [ username, setUsername] = useState("")
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchUsers())
  }, [])

  return (
    <>
      <input onChange={e => setUsername(e.target.value)}/>
      <button onClick={() => dispatch(addUser({ id: 20, username}))}>Add user</button>
      { loadingUsers ? <p>Loading...</p> : ""}
      { error !== null ? <p>An error occurred</p> : users.map(x => <p key={x.id}>{x.username}</p>)}
    </>
  );
}

export default App;
