import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { API_BASE_URL } from "../constants";

export const fetchUsers = createAsyncThunk(
    "user/fetchUsers",
    async () => {
        const response = await fetch(API_BASE_URL)

        if (!response.ok) {
            return new Promise.reject()
        }

        const users = await response.json()
        return { users }
    }
)

export const addUser = createAsyncThunk(
    "user/addUser",
    async (userDetails) => {
        const response = await fetch(API_BASE_URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(userDetails)
        })

        const user = await response.json()
        return { user }
    }
)

const simulateDelay = async () => {
    return new Promise(x => setTimeout((x), 2000))
}

export const userSlice = createSlice({
    name: "user",
    initialState: {
        users: [],
        loadingUsers: false,
        error: null
    },
    reducers: {
    },
    extraReducers: {
        [fetchUsers.pending]: (state, action) => {
            state.loadingUsers = true
        },
        [fetchUsers.fulfilled]: (state, action) => {
            state.users = action.payload.users
            state.loadingUsers = false
        },
        [fetchUsers.rejected]: (state, action) => {
            state.error = action.error
            state.loadingUsers = false
        },
        [addUser.fulfilled]: (state, action) => {
            state.users.push(action.payload.user)
        } 
    }
})

export const { } = userSlice.actions
export default userSlice.reducer